<html>
 <head>
 <title>Contoh Pengambilan Data Jamak</title>
 </head>
 <body>
 <h2>Daftar Biodata</h2>
 <p>[ <a href="lat10.html">Tambah Data</a> ] </p>
 <table width="650px" border="1">
 <tr style="background:#ccc">
 <th width="10%">ID</th>
 <th width="22%">Nama</th>
 <th width="50%">Alamat</th>
 <th>Aksi</th>
 </tr>
 <?php

 include("tes.php");

 $sql = "SELECT id,nama,alamat FROM biodata";
 $hasil = mysqli_query($koneksi, $sql) or exit("Error query: <b>".$sql."</b>.");

 while($data = mysqli_fetch_assoc($hasil)){
 ?>
 <tr>
 <td align="center"><?php echo $data['id']; ?></td>
 <td><?php echo $data['nama']; ?></td>
 <td><?php echo $data['alamat']; ?></td>
 <td>
 <a href="lat6.php?id=<?php echo $data['id']; ?>">
 Detail
</a>
 <a href="lat12.php?id=<?php echo $data['id']; ?>">
 Ubah
</a>
 <a href="lat14.php?id=<?php echo $data['id']; ?>">
 Hapus
</a>
 </td>
 </tr>
 <?php
 }

 ?>
 </table>
 </body>
</html>
